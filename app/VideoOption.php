<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class VideoOption extends BaseModel
{
    
    protected $fillable = [
    	'autoplay',

        'loop',

        'controls',

        'mute',

        'fullscreen',

        'frameborder',

        'related_videos',

        'gyroscope',

        'accelerometer',

        'picture',

        'encrypt_media',

        'start',

        'portrait',

        'title',

        'byline',
    	];
    
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }
}
